#!/bin/sh

## \package    chOP
#  \author     viharm
#  \version    See VERSION
#  \brief      POSIX script to set ownership and permissions
#  \detail     A script which uses a directory/file owner and permissions map
#              to set ownership and permissions as per a supplied configuration file
#  \copyright  Copyright (c) 2015, viharm
#  \license    Modified BSD (3-clause) licence.

# Preset default exit status
nm_ExitStatus=$((1))

# Set debugging mode; 0: normal (default), 1: debug
bl_Debug=$((0))

# Set debugging-to-syslog mode; 0: normal (default), 1: debug to syslog
bl_Syslog=$((0))

# Set log file compression to 0
bl_LogfileBzip2=$((0))
bl_LogfileXz=$((0))
bl_LogfileGzip=$((0))

# Debug function
fn_Debug() {
  if test $bl_Debug = 1; then
    if test -n "$fl_Log"; then
      echo "$@" >> "$fl_Log"
    elif test $bl_Syslog = 1; then
      logger -i -s -p local6.info "$@" -t "$(basename "$0")"
      # if this is selected then shell default behaviour may be to output to console too;
    else
      echo "$@"
    fi
  fi
}

# Verify and prepare log file
fn_Log() {
  fl_Log=$1
  fn_Debug "Log file requested is $fl_Log"
  fn_Debug "Verifying log file $fl_Log"
  if test -e "$fl_Log"; then
  	fn_Debug "Log location $fl_Log exists"
  	fn_Debug "Checking $fl_Log type"
  	if test -f "$fl_Log"; then
  	  fn_Debug "$fl_Log is a regular file"
  	else
  	  fn_Debug "$fl_Log is not a regular file; resetting variable"
  	  fl_Log=""
  	  nm_ExitStatus=$((12))
  	fi
  else
  	fn_Debug "Log location $fl_Log not found, creating empty"
  	touch $fl_Log
  	if test $(($?)) -ne 0; then
  	  fn_Debug "Failed to create log file $fl_Log; resetting variable"
  	  fl_Log=""
  	  nm_ExitStatus=$((12))
  	fi
  fi
  if test $nm_ExitStatus -ne $((12)); then
	  fn_Debug "Initialising log file $fl_Log"
	  echo "_______________________" >> $fl_Log
	  fn_Version >> $fl_Log
	  nm_ExitStatus=$((0))
    echo "------------------" >> "$fl_Log"
 	  date +%Y-%m-%dT%H:%M:%S%z >> "$fl_Log"
 	  echo "------------------" >> "$fl_Log"
  fi
}

# Create software info
nm_Counter_01=$((0))
while read sr_VersionItem; do
  nm_Counter_01=$((nm_Counter_01+1))
  fn_Debug "Reading line $nm_Counter_01: $sr_VersionItem"
  case $nm_Counter_01 in
    1) sr_PkgVersion=$sr_VersionItem; fn_Debug "Package version set: $sr_PkgVersion" ;;
    2) sr_PkgDate="$(echo "$sr_VersionItem" | cut -d '(' -f 2 | cut -d ')' -f 1)"; fn_Debug "Package date set: $sr_PkgDate" ;;
  esac
done < "$(dirname "$0")/VERSION"
fn_Debug "$nm_Counter_01 line(s) read from version file; deleting counter."
unset nm_Counter_01
sr_PkgAuthor='viharm'

# Exit function
fn_Finish() {
  fn_Debug "Exiting with status $nm_ExitStatus"
  if test $bl_LogfileGzip = 1; then
    fn_Debug "Request to compress with Gzip found. Checking for command"
    command -v gzip
    if test $(($?)) -eq 0; then
      fn_Debug "Gzip found; attempting to compress"
      gzip $fl_Log
    else
      fn_Debug "Gzip not found"
    fi
  elif test $bl_LogfileBzip2 = 1; then
    fn_Debug "Request to compress with Bzip2 found. Checking for command"
    command -v bzip2
    if test $(($?)) -eq 0; then
      fn_Debug "Bzip2 found; attempting to compress"
      bzip2 $fl_Log
    else
      fn_Debug "Bzip2 not found"
    fi
  elif test $bl_LogfileXz = 1; then
    fn_Debug "Request to compress with XZ found. Checking for command"
    command -v xz
    if test $(($?)) -eq 0; then
      fn_Debug "XZ found; attempting to compress"
      xz $fl_Log
    else
      fn_Debug "xz not found"
    fi
  fi
  exit $nm_ExitStatus
}

# Help function
fn_Help() {
	echo "$(basename "$0")": ERROR: "$@" 1>&2
	echo usage: "$(basename "$0")" '[ -v ] [ -d ( [ -s ] | [ -o /log/file/name ] ) ] ( [-l /target/location -u user -g group -p permissions -f filepermissions] | [-m /map/file] )'
	nm_ExitStatus=$((2))
	fn_Finish
}

# Function to verify target location
fn_Location() {
  fl_Locn=$1
  fn_Debug "Location is $fl_Locn"
  fn_Debug "Verifying location $fl_Locn"
  if test -e "$fl_Locn"; then
  	fn_Debug "Location $fl_Locn exists"
  	fn_Debug "Checking $fl_Locn type"
  	if test -d "$fl_Locn"; then
  	  fn_Debug "$fl_Locn is a directory"
  	  sr_PermMode="Dir"
  	elif test -f "$fl_Locn"; then
  	  fn_Debug "$fl_Locn is a regular file"
  	  sr_PermMode="File"
  	fi
  else
  	fn_Debug "Location $fl_Locn not found"
  	nm_ExitStatus=$((3))
  fi
}

# Function to verify owner username
fn_User() {
  sr_User=$1
  fn_Debug "User is $sr_User"
  fn_Debug "Checking if user $sr_User exists"
  if id -u "$sr_User" >/dev/null 2>&1; then
  	fn_Debug "User $sr_User exists"
  else
  	fn_Debug "User $sr_User not found"
  	fn_Debug "Checking for escaped user"
  	if test "$sr_User" = "-"; then
  	  fn_Debug "User escaped"
  	  sr_User=""
  	  fn_Debug "User blanked: $sr_User"
  	else
  	  fn_Debug "User not escaped; User $sr_User unidentified"
  	  nm_ExitStatus=$((4))
  	fi
  fi
}

# Function to verify owner group
fn_Group() {
  sr_Group=$1
  fn_Debug "Group is $sr_Group"
  fn_Debug "Checking if group $sr_Group exists"
  if getent group "$sr_Group" >/dev/null 2>&1; then
  	fn_Debug "Group $sr_Group exists"
  else
  	fn_Debug "Group $sr_Group not found"
  	fn_Debug "Checking for escaped group"
  	if test "$sr_Group" = "-"; then
  	  fn_Debug "Group escaped"
  	  sr_Group=""
  	  fn_Debug "Group blanked: $sr_Group"
  	else
  	  fn_Debug "Group not escaped; Group $sr_Group unidentified"
  	  nm_ExitStatus=$((5))
  	fi
  fi
}

# Function to verify permissions 
fn_Perm() {
  sr_Perm=$1
  fn_Debug "Permissions are $sr_Perm"
  fn_Debug "Checking if permissions $sr_Perm are readable"
  if test -z "$sr_Perm"; then
  	fn_Debug "Permission $sr_Perm are null"
  	nm_ExitStatus=$((0))
  else
  	fn_Debug "Permissions $sr_Perm are non-null"
  	fn_Debug "Checking string length of permissions"
  	if test ${#sr_Perm} -eq 3; then
  	  fn_Debug "String length is 3, cycling through each permission flag"
  	  for sr_PermIndividual in "$(echo "$sr_Perm" | cut -b 1)" "$(echo "$sr_Perm" | cut -b 2)" "$(echo "$sr_Perm" | cut -b 3)"; do
  	    case $sr_PermIndividual in
  	      [01234567]) fn_Debug "$sr_PermIndividual is a valid permission flag";;
  	      *) fn_Debug "$sr_PermIndividual is an invalid permission flag"; nm_ExitStatus=$((6));;
  	    esac
  	  done
  	else
  	  fn_Debug "Permission string is not three characters wide"
  	  nm_ExitStatus=$((6))
  	fi
  fi
}

# Function to verify map file containing parameter values per line
fn_Map() {
  fl_Map=$1
  fn_Debug "Map file is $fl_Map"
  fn_Debug "Checking if map file $fl_Map exists"
  if test -f "$fl_Map"; then
  	fn_Debug "Map file $fl_Map exists"
  	# Now sanity check all lines of the map file to identify any bad lines
    fn_Debug "Verifying map file $fl_Map"
    nm_Counter_01=$((0))
    while read sr_PermMapItemLocation sr_PermMapItemUser sr_PermMapItemGroup sr_PermMapItemPerm sr_PermMapItemFilePerm sr_PermMapItemDiscard; do
      nm_Counter_01=$((nm_Counter_01+1))
      fn_Debug "Reading line $nm_Counter_01: $sr_PermMapItemLocation $sr_PermMapItemUser $sr_PermMapItemGroup $sr_PermMapItemPerm $sr_PermMapItemFilePerm"
      if test -z "$sr_PermMapItemLocation"; then
        fn_Debug "Line is empty, skipping iteration"
        continue
      else
        fn_Debug "Line is not empty, continuing iteration"
      fi
      sr_CommentChecker="${sr_PermMapItemLocation%"${sr_PermMapItemLocation#?}"}"
      fn_Debug "First character is $sr_CommentChecker"
      if test "$sr_CommentChecker DummyText" = "# DummyText"; then
        fn_Debug "Line is commented, skipping iteration"
        continue
      else
        fn_Debug "Line is not commented, continuing iteration"
      fi
      fn_Location "$sr_PermMapItemLocation"
      fn_Debug "Interrogating error status: $nm_ExitStatus"
      if test $nm_ExitStatus -eq 0; then
        fn_User "$sr_PermMapItemUser"
      fi
      fn_Debug "Interrogating error status: $nm_ExitStatus"
      if test $nm_ExitStatus -eq 0; then
        fn_Group "$sr_PermMapItemGroup"
      fi
      fn_Debug "Interrogating error status: $nm_ExitStatus"
      if test $nm_ExitStatus -eq 0; then
        fn_Perm "$sr_PermMapItemPerm"
      fi
      fn_Debug "Interrogating error status: $nm_ExitStatus"
      if test $nm_ExitStatus -eq 0; then
        fn_FilePerm "$sr_PermMapItemFilePerm"
      fi
      fn_Debug "Interrogating error status: $nm_ExitStatus"
      if test $nm_ExitStatus -eq 0; then
        fn_Debug "Map file line is clean"
      else
        fn_Debug "Error found in buffer whilst processing map file line $nm_Counter_01"
      	nm_ExitStatus=$((7))
      fi
    done < "$fl_Map"
    fn_Debug "$nm_Counter_01 line(s) read from map file; deleting counter."
    unset nm_Counter_01
    unset sr_CommentChecker
  else
  	fn_Debug "Map file $fl_Map not found"
  	nm_ExitStatus=$((7))
  fi
}

# Function to parse separate permissions for file objects in given target location
fn_FilePerm() {
  sr_FilePerm=$1
  fn_Debug "File permissions are $sr_FilePerm"
  fn_Debug "Checking if file permissions $sr_FilePerm are readable"
  if test -z "$sr_FilePerm"; then
  	fn_Debug "File permission $sr_FilePerm are null"
  	nm_ExitStatus=$((0))
  else
  	fn_Debug "File permissions $sr_FilePerm are non-null"
  	fn_Debug "Checking string length of file permissions"
  	if test ${#sr_FilePerm} -eq 3; then
  	  fn_Debug "String length is 3, cycling through each file permisson flag"
  	  for sr_FilePermIndividual in "$(echo "$sr_FilePerm" | cut -b 1)" "$(echo "$sr_FilePerm" | cut -b 2)" "$(echo "$sr_FilePerm" | cut -b 3)"; do
  	    case $sr_FilePermIndividual in
  	      [01234567]) fn_Debug "$sr_FilePermIndividual is a valid file permission flag";;
  	      *) fn_Debug "$sr_FilePermIndividual is an invalid file permission flag"; nm_ExitStatus=$((13));;
  	    esac
  	  done
  	else
  	  fn_Debug "File permission string is not three characters wide"
  	  nm_ExitStatus=$((13))
  	fi
  fi
}

# Function to display version information
fn_Version() {
  echo "$(basename "$0") version $sr_PkgVersion"
  echo "Modified $sr_PkgDate"
  echo "Copyright (c) "$(date '+%Y')", $sr_PkgAuthor"
  echo 'Licensed under modified BSD (3-clause) licence'
  nm_ExitStatus=$((8))
}

# Function to set owner user and group for the target
fn_Own() {
  fn_Debug "Formulating options"
  sr_Options="-R -h"
  if test $bl_Debug -ne 0; then
    sr_Options="$sr_Options -v"
  fi
  fn_Debug "Proposed options string is $sr_Options"
  fn_Debug "Formulating owner"
  fn_Debug "Checking user & group"
  if test -z $sr_User; then
    fn_Debug "User not found"
    if test -z $sr_Group; then
      fn_Debug "Neither user nor group found"
      fn_Debug "Retaining current ownership"
      return $((0))
    else
      fn_Debug "Group $sr_Group found"
      sr_Owner=":$sr_Group"
    fi
  else
    fn_Debug "User $sr_User found"
    if test -z $sr_Group; then
      fn_Debug "Group not found"
      sr_Owner="$sr_User"
    else
      fn_Debug "User $sr_User and group $sr_Group found"
      sr_Owner="$sr_User:$sr_Group"
    fi
  fi
  fn_Debug "Proposed owner is $sr_Owner"
  fn_Debug "Checking if log file is provided"
  if test -z $fl_Log; then
    fn_Debug "Setting ownership: chown $sr_Options -- $sr_Owner $fl_Locn"
    chown $sr_Options -- "$sr_Owner" "$fl_Locn"
  else
    fn_Debug "Setting ownership: chown $sr_Options -- $sr_Owner $fl_Locn >> $fl_Log 2>&1"
    chown $sr_Options -- "$sr_Owner" "$fl_Locn" >> "$fl_Log" 2>&1
  fi
  if test $(($?)) -eq 0; then
    return $((0))
  else
    return $((9))
  fi
}

# Function to set permissions for the target
fn_SetPerm() {
  if test -z "$sr_Perm"; then
    fn_Debug "Null permission request, retaining permissions"
    fn_Debug "Returning non-zero exit status to avoid next step of removing executable bit"
    return $((10))
  else
    fn_Debug "Formulating options"
    sr_Options="-R"
    if test $bl_Debug -ne 0; then
      sr_Options="$sr_Options -v"
    fi
    fn_Debug "Proposed options string is $sr_Options"
    fn_Debug "Checking if log file is provided"
    if test -z $fl_Log; then
      fn_Debug "Setting permissions: chmod $sr_Options -- $sr_Perm $fl_Locn"
      chmod $sr_Options -- "$sr_Perm" "$fl_Locn"
    else
      fn_Debug "Setting permissions: chmod $sr_Options -- $sr_Perm $fl_Locn >> $fl_Log 2>&1"
      chmod $sr_Options -- "$sr_Perm" "$fl_Locn" >> "$fl_Log" 2>&1
    fi
    if test $(($?)) -eq 0; then
      return $((0))
    else
      return $((10))
    fi
  fi
}

# Function to remove executable permissions from all files in target
fn_ExecRem() {
  fn_Debug "Verifying permissions mode: $sr_PermMode"
  if test "$sr_PermMode" = "Dir"; then
    if test -z "$sr_FilePerm"; then
      fn_Debug "Formulating options"
      sr_Options=""
      if test $bl_Debug -ne 0; then
        sr_Options="$sr_Options -v"
      fi
      fn_Debug "Proposed options string is $sr_Options"
      fn_Debug "Checking if log file is provided"
      if test -z "$fl_Log"; then
        find "$fl_Locn" -type f | while read sr_FileItem; do
          fn_Debug "Found file $sr_FileItem"
          fn_Debug "Removing executable bit: chmod $sr_Options -- a-x $sr_FileItem"
          chmod $sr_Options -- a-x "$sr_FileItem"
        done
      else
        find "$fl_Locn" -type f | while read sr_FileItem; do
          fn_Debug "Found file $sr_FileItem"
          fn_Debug "Removing executable bit: chmod $sr_Options -- a-x $sr_FileItem >> $fl_Log 2>&1"
          chmod $sr_Options -- a-x "$sr_FileItem" >> "$fl_Log" 2>&1
        done
      fi
      if test $(($?)) -eq 0; then
        return $((0))
      else
        return $((11))
      fi
    else
      fn_Debug "Specific file permissions requested, removal of executable permissions skipped"
    fi
  else
    fn_Debug "Permissions mode is not set for directory; removal of executable permissions skipped"
  fi
}

# Function to set spefici permissions for file objects in the target
fn_SetFilePerm() {
  fn_Debug "Verifying permissions mode: $sr_PermMode"
  if test "$sr_PermMode" = "Dir"; then
    if test -z "$sr_FilePerm"; then
      fn_Debug "Null file permission request, retaining file permissions"
    else
      fn_Debug "Formulating options"
      sr_Options=""
      if test $bl_Debug -ne 0; then
        sr_Options="$sr_Options -v"
      fi
      fn_Debug "Proposed options string is $sr_Options"
      fn_Debug "Checking if log file is provided"
      if test -z $fl_Log; then
        find "$fl_Locn" -type f | while read sr_FileItem; do
          fn_Debug "Found file $sr_FileItem"
          fn_Debug "Setting file permissions: chmod $sr_Options -- $sr_FilePerm $sr_FileItem"
          chmod $sr_Options -- $sr_FilePerm "$sr_FileItem"
        done
      else
        find "$fl_Locn" -type f | while read sr_FileItem; do
          fn_Debug "Found file $sr_FileItem"
          fn_Debug "Setting file permissions: chmod $sr_Options -- $sr_FilePerm $sr_FileItem >> $fl_Log 2>&1"
          chmod $sr_Options -- $sr_FilePerm "$sr_FileItem" >> "$fl_Log" 2>&1
        done
      fi
      if test $(($?)) -eq 0; then
        return $((0))
      else
        return $((14))
      fi
    fi
  else
    fn_Debug "Permissions mode is not set for directory; setting of specific file permissions skipped"
  fi
}

# Function to process an individual set of owner-permissions
fn_ProcessSet() {
  fn_Debug "Checking for existing errors in error buffer: $nm_ExitStatus"
  if test $nm_ExitStatus -le 1; then
    fn_Debug "No specific errors found, processing parameters"
    fn_Own
    nm_ExitStatus=$(($?))
    fn_Debug "Outcome of ownership function: $nm_ExitStatus"
    fn_Debug "Checking error status before setting permissions: $nm_ExitStatus"
    if test $nm_ExitStatus -eq 0; then
      fn_Debug "Error status $nm_ExitStatus"
      fn_SetPerm
      nm_ExitStatus=$(($?))
      fn_Debug "Outcome of permissions function is $nm_ExitStatus"
    else
      fn_Debug "Error $nm_ExitStatus whilst setting ownership"
    fi
    fn_Debug "Checking error status before removing executable permissions: $nm_ExitStatus"
    if test $nm_ExitStatus -eq 0; then
      fn_Debug "Error status $nm_ExitStatus"
      fn_ExecRem
      nm_ExitStatus=$(($?))
      fn_Debug "Outcome of function to remove file executable permissions is $(($?))"
    else
      fn_Debug "Error $nm_ExitStatus whilst setting permissions"
    fi
    fn_Debug "Checking error status before processing specific file permissions: $nm_ExitStatus"
    if test $nm_ExitStatus -eq 0; then
      fn_Debug "Error status $nm_ExitStatus"
      fn_SetFilePerm
      nm_ExitStatus=$(($?))
      fn_Debug "Outcome of file permissions function is $(($?))"
    else
      fn_Debug "Error $nm_ExitStatus whilst removing file executable permissions"
    fi
  else
    fn_Debug "Errors found; skipping processing of set"
  fi
}

# Function to read lines of the map file and send them to different functions to parse and process
fn_ProcessMap() {
  fn_Debug "Reading map file $fl_Map"
  nm_Counter_01=$((0))
  while read sr_PermMapItemLocation sr_PermMapItemUser sr_PermMapItemGroup sr_PermMapItemPerm sr_PermMapItemFilePerm sr_PermMapItemDiscard; do
    nm_Counter_01=$((nm_Counter_01+1))
    fn_Debug "Reading line $nm_Counter_01: $sr_PermMapItemLocation $sr_PermMapItemUser $sr_PermMapItemGroup $sr_PermMapItemPerm $sr_PermMapItemFilePerm"
    if test -z "$sr_PermMapItemLocation"; then
      fn_Debug "Line is empty, skipping iteration"
      continue
    else
      fn_Debug "Line is not empty, continuing iteration"
    fi
    sr_CommentChecker="${sr_PermMapItemLocation%"${sr_PermMapItemLocation#?}"}"
    fn_Debug "First character is $sr_CommentChecker"
    if test "$sr_CommentChecker DummyText" = "# DummyText"; then
      fn_Debug "Line is commented, skipping iteration"
      continue
    else
      fn_Debug "Line is not commented, continuing iteration"
    fi
    fn_Location "$sr_PermMapItemLocation"
    fn_Debug "Interrogating error status: $nm_ExitStatus"
    if test $nm_ExitStatus -eq 0; then
      fn_User "$sr_PermMapItemUser"
    fi
    fn_Debug "Interrogating error status: $nm_ExitStatus"
    if test $nm_ExitStatus -eq 0; then
      fn_Group "$sr_PermMapItemGroup"
    fi
    fn_Debug "Interrogating error status: $nm_ExitStatus"
    if test $nm_ExitStatus -eq 0; then
      fn_Perm "$sr_PermMapItemPerm"
    fi
    fn_Debug "Interrogating error status: $nm_ExitStatus"
    if test $nm_ExitStatus -eq 0; then
      fn_FilePerm "$sr_PermMapItemFilePerm"
    fi
    fn_Debug "Interrogating error status: $nm_ExitStatus"
    if test $nm_ExitStatus -eq 0; then
      fn_Debug "Previous permissions overridden by those on map line $nm_Counter_01"
      fn_Debug "Processing set"
      fn_ProcessSet
    fi
  done < "$fl_Map"
  fn_Debug "$nm_Counter_01 line(s) read from map file; deleting counter."
  unset nm_Counter_01
  unset sr_CommentChecker
}

fn_Debug "$# arguments provided"

# Initial loop to read supplied arguments
while :
do
  fn_Debug "Analysing $1"
  if test -z "$1"; then
  	fn_Debug "No more arguments to analyse"
  	break;
  else
    case "$1" in
      -v) fn_Version;;
      -d) bl_Syslog=$((0)); bl_Debug=$((1));;
      -s) bl_Debug=$((1)); bl_Syslog=$((1));;
      -o) shift; fn_Log "$1";;
      -z) bl_LogfileBzip2=$((0)); bl_LogfileXz=$((0)); bl_LogfileGzip=$((1));;
      -j) bl_LogfileGzip=$((0)); bl_LogfileXz=$((0)); bl_LogfileBzip2=$((1));;
      -J) bl_LogfileBzip2=$((0)); bl_LogfileGzip=$((0)); bl_LogfileXz=$((1));;
      -l) shift; fn_Location "$1";;
      -u) shift; fn_User "$1";;
      -g) shift; fn_Group "$1";;
      -p) shift; fn_Perm "$1";;
      -m) shift; fn_Map "$1";;
      -f) shift; fn_FilePerm "$1";;
      --) shift; break;;
      -*) fn_Help "bad argument $1";;
      *) fn_Help "bad argument $1"; break;;
    esac
    shift
  fi
done

fn_Debug "Checking if map file is provided: $fl_Map"
if test -z "$fl_Map"; then
  fn_Debug "Map file not provided, processing supplied arguments"
  fn_ProcessSet
else
  fn_Debug "Map file provided, ignoring other supplied arguments"
  fn_ProcessMap
fi

fn_Debug "Complete"
fn_Finish