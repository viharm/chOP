**chOP**

|           |                                                                         |
|:----------|:------------------------------------------------------------------------|
| Version   | 1.2.0                                                                   |
| Changes   | https://gitlab.com/viharm/chOP/merge_requests/5/commits                 |
| Download  | https://gitlab.com/viharm/chOP/tags                                     |
| Issues    | https://gitlab.com/viharm/chOP/issues                                   |
| License   | BSD (3-clause)                                                          |
| Language  | [POSIX Bourne shell](http://pubs.opengroup.org/onlinepubs/9699919799/)  |

POSIX script to set ownership and permissions

*chOP* is a POSIX script to set owner, group and permissions recursively; and then remove executable permissions from files.

Optionally, it can process several directories (recursively) and files by reading them from a ownership-permissions map.


# Installation

No installation required.


## Pre-requisites

* Standard POSIX commands
    * `test`
    * `chown`
    * `chmod`
    * `find`
    * `if`-`elif`-`else`-`fi`
    * `while`-`do`-`done`
    * `case`-`in`-`esac`
    * `cut`
    * `date`
    * `read`
    * `unset`
    * `echo`
    * `logger`
    * `return`
    * `exit`
* Optional tools
    * `gzip`
    * `bzip2`
    * `xz`


## Download


### Files archive

Get the release archives from the download link provided at the top of this page.


### Clone

Clone repository.

```
git clone https://gitlab.com/viharm/chOP.git
```


## Deploy

Save the downloaded directory structure in your choice of path within your application.


# Configuration

No specific configuration required yet.


# Usage

*chOP* can be used to set ownership and/or permissions for filesystem objects recursively.

As a security feature it automatically removes executable permissions from file objects recursively.

It can process separate permissions for file objects if desired.

*chOP* works on the following logic:

1. Sets ownership (user and/or group) recursively
2. Sets permissions on all filesystem objects recursively
3. If separate file object permission note provided, then it removes executable permission from all file objects
4. If provided, sets file object permissions recursively

*chOP* accepts all necessary parameters in the command line for basic one-shot usage.

For more complex directory-owner-group-permission maps, a simple configuration file can be specified.

Pass the following arguments to *chOP* in the command line

|       Argument        |                       Description                              | Optional | Default |
| --------------------- | -------------------------------------------------------------- |:--------:|:-------:|
| `-d`                  |Debug (output to console by default)                            |   Yes    |   None  |
| `-s`                  |Debug to *syslog* (requires `-d`)                               |   Yes    |   None  |
| `-o /path/debug.log`  |Debug to file `/path/debug.log`                                 |   Yes    |   None  |
| `-z`                  |Compress log file with `gzip`. Ignored if `-o` not given        |   Yes    |   None  |
| `-j`                  |Compress log file with `bzip2`. Ignored if `-o` not given       |   Yes    |   None  |
| `-J`                  |Compress log file with `xz`. Ignored if `-o` not given          |   Yes    |   None  |
| `-l /target/location` |Filesystem object (file or directory) to process                |    No    |   None  |
| `-u username`         |Username to assign ownership of the above filesystem object to  |   Yes    | Retain  |
| `-g group`            |Group that the above filesystem object must belong to           |   Yes    | Retain  |
| `-p ugo`              |3-digit permission map as per short POSIX notation              |   Yes    | Retain  |
| `-f ugo`              |3-digit permission map for file objects in above location       |   Yes    |  `a-x`  |
| `-m /path/perms.conf` |Ownership and permission map for multiple locations             |   Yes    |   None  |
| `-v`                  |Display version information                                     |   Yes    |   None  |


## Basic usage

Although *chOP* allows operation in various scenarios, the most common usage involves passing the location (`-u`), user (`-u`), group (`-g`) and permissions (`-p`).

```
/install/location/chOP/chOP.sh -l /var/www -u root -g www-data -p 770
```

In its most basic usage, the location can be speciied with no further arguments. This retains the ownership and permissions, but removes the executable bit from all files in the specified location recusrsively.


## Advanced usage

Complex ownership and permission structure can be achieved by supplying *chOP* with a map of target filesystem objects with the desired owner, group and permissions; all delimited on a single line with shite space (`tab` or `space`).

*chOP* must be invoked with the `-m` argument by specifying the location of the map file, e.g., `-m /map/file`.


### Map file structure

The structure of the ownership and permissions map in a map file is as follows

```
/path/to/target/directory root www-data 775 640
```


#### Location

The real path of the object to be processed. This can be any file system object subject to ownership and permissions.

Currently *chOP* primarily processes directory and file objects. Any other file system objects like sockets and devices are treated as file objects.

From the perspective of ownership and permissions this is considered as a resonable default.


#### Owner

The specifies the target owner username of the filesystem object. This user must exist.

If a non-existent user is specified, the current owner of the target location is retained (recursively). If this is intentional, the use of the minus sign (`-`) is encouraged.


#### Group

The specifies the target group of the filesystem object. This group must exist.

If a non-existent group is specified, the current owner of the target location is retained (recursively). If this is intentional, the use of the minus sign (`-`) is encouraged.


#### Filesystem object permissions

3-digit permission map as per short POSIX notation.

Example: `750` (Owner: read, write, execute; Group: read, execute; Other: none)

This is not optional, and must be a valid *POSIX* permission specification.


#### File object permissions

This field allows specification of permissions file objects within the primary location if the primary location is a directory.

If this is not specified, *chOP* applies read and write permissions as per the direcoty object permission specification, but removes the executable permissions from all filesystem objects, as a security measure.

WARNING: This default behaviour can cause many applications to malfunction, which rely on certain executable files.


### Multiple maps

For multiple locations, each must be specified on an individual line.

Lines starting with `#` are treated as comments and are ignored.

Empty lines are ignored.


### Duplicate arguments

If a map file is provided, *chOP* ignores ownership and permission requests made via arguments in the command line.


## Examples


### Ad-hoc operation

```
/install/location/chOP/chOP.sh -l /var/www -u root -g www-data -p 770
```


### Multiple targets

```
/install/location/chOP/chOP.sh -m /home/myuser/permmap.conf
```

Where `permmap.conf` contains...

```
/var/www             root  www-data  750
/var/www/pydio/data  root  www-data  770  664
```


# Known limitations

* *chOP* processes directories recursively by default
    * For specific file permissions separate lines need to be added **after** the parent directories to override previously set owner-group-permissions.

Additionally see

* [Open issues](https://gitlab.com/viharm/chOP/issues?state=opened)
* [Issues which won't be fixed](https://gitlab.com/viharm/chOP/issues?scope=all&state=all&label_name[]=Outcome_Wontfix)


# Support

For issues, queries, suggestions and comments please create an issue by using the link provided at the top of this page.

Please remember to debug first.


## Debugging

Debugging can be enabled by doing one of the following
*  Passing `-d` as an argument, preferably the first, or
*  Setting variable `bl_Debug` to `1` in the script like: `bl_Debug=$((1))`


### Debug output

The debug output is directed to the console by default. However there are options for saving the debug log to file or to the *syslog*.


#### Debug to file

If this debug output is desired to be saved to a log file, then pass `-o` as a parameter and provide the location od a log file as its value (delimited as usual by whitespace).
   `/install/location/chOP/chOP.sh -d -o /path/to/logfile.log -l /var/www -u root -g www-data -p 770 -f 664`

Additionally, the log file can be compressed immediately after running by using arguments `-z` (*GZip*), `-j` (*BZip2*), or `-J` (*XZ*).


#### Debug to syslog

Debugging to syslog can be enabled by doing one of the following
*  Passing `-s` as an argument, preferably the first (requires `-d`), or
*  Setting variable `bl_Syslog` to `1` in the script like: `bl_Syslog=$((1))`

Please remember to enable debugging (`-d`) separately for debugging to *syslog*.


# Contribute

Please feel free to clone/fork and contribute via pull requests. Donations also welcome, simply create an issue by using the link provided at the top of this page.

Please make contact for more information.


# Environments

Developed on..

* *Debian Wheezy*
* *Debian Jessie*
* *Debian Stretch*

Known to be working on 

* *Debian Wheezy*
* *Debian Jessie*
* *Debian Stretch*
* *FreeBSD 11.1-RELEASE*


# Licence

Licensed under the modified BSD (3-clause) licence.

Copyright (c) 2015~17, Viharm
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


# Credits


#### Codiad

*Codiad* web based IDE (https://github.com/Codiad/Codiad), used under a MIT-style license.

Copyright (c) Codiad & Kent Safranski (codiad.com)


#### CodeGit

*CodeGit* *Git* plugin for *Codiad* (https://github.com/Andr3as/Codiad-CodeGit), used under a MIT-style license.

Copyright (c) Andr3as <andranode@gmail.com>


#### Ungit

*Ungit* client for *Git* (https://github.com/FredrikNoren/ungit) used under the MIT license

Copyright (C) Fredrik Norén


## jEdit

*jEdit* text editor (http://www.jedit.org/), used under the GNU GPL v2.

Copyright (C) jEdit authors.


## GitLab

Hosted by *GitLab* code repository (gitlab.com).

